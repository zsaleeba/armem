
tests/hello:     file format elf32-littlearm


Disassembly of section .init:

000003a0 <_init>:
 3a0:	e92d4008 	push	{r3, lr}
 3a4:	eb00002b 	bl	458 <call_weak_fn>
 3a8:	e8bd8008 	pop	{r3, pc}

Disassembly of section .plt:

000003ac <.plt>:
 3ac:	e52de004 	push	{lr}		; (str lr, [sp, #-4]!)
 3b0:	e59fe004 	ldr	lr, [pc, #4]	; 3bc <.plt+0x10>
 3b4:	e08fe00e 	add	lr, pc, lr
 3b8:	e5bef008 	ldr	pc, [lr, #8]!
 3bc:	00010c08 	.word	0x00010c08

000003c0 <__cxa_finalize@plt>:
 3c0:	e28fc600 	add	ip, pc, #0, 12
 3c4:	e28cca10 	add	ip, ip, #16, 20	; 0x10000
 3c8:	e5bcfc08 	ldr	pc, [ip, #3080]!	; 0xc08

000003cc <puts@plt>:
 3cc:	e28fc600 	add	ip, pc, #0, 12
 3d0:	e28cca10 	add	ip, ip, #16, 20	; 0x10000
 3d4:	e5bcfc00 	ldr	pc, [ip, #3072]!	; 0xc00

000003d8 <__libc_start_main@plt>:
 3d8:	e28fc600 	add	ip, pc, #0, 12
 3dc:	e28cca10 	add	ip, ip, #16, 20	; 0x10000
 3e0:	e5bcfbf8 	ldr	pc, [ip, #3064]!	; 0xbf8

000003e4 <__gmon_start__@plt>:
 3e4:	e28fc600 	add	ip, pc, #0, 12
 3e8:	e28cca10 	add	ip, ip, #16, 20	; 0x10000
 3ec:	e5bcfbf0 	ldr	pc, [ip, #3056]!	; 0xbf0

000003f0 <abort@plt>:
 3f0:	e28fc600 	add	ip, pc, #0, 12
 3f4:	e28cca10 	add	ip, ip, #16, 20	; 0x10000
 3f8:	e5bcfbe8 	ldr	pc, [ip, #3048]!	; 0xbe8

Disassembly of section .text:

000003fc <main>:
 3fc:	4803      	ldr	r0, [pc, #12]	; (40c <main+0x10>)
 3fe:	b508      	push	{r3, lr}
 400:	4478      	add	r0, pc
 402:	f7ff efe4 	blx	3cc <puts@plt>
 406:	2000      	movs	r0, #0
 408:	bd08      	pop	{r3, pc}
 40a:	bf00      	nop
 40c:	0000016c 	.word	0x0000016c

00000410 <_start>:
 410:	f04f 0b00 	mov.w	fp, #0
 414:	f04f 0e00 	mov.w	lr, #0
 418:	bc02      	pop	{r1}
 41a:	466a      	mov	r2, sp
 41c:	b404      	push	{r2}
 41e:	b401      	push	{r0}
 420:	f8df a024 	ldr.w	sl, [pc, #36]	; 448 <_start+0x38>
 424:	a308      	add	r3, pc, #32	; (adr r3, 448 <_start+0x38>)
 426:	449a      	add	sl, r3
 428:	f8df c020 	ldr.w	ip, [pc, #32]	; 44c <_start+0x3c>
 42c:	f85a c00c 	ldr.w	ip, [sl, ip]
 430:	f84d cd04 	str.w	ip, [sp, #-4]!
 434:	4b06      	ldr	r3, [pc, #24]	; (450 <_start+0x40>)
 436:	f85a 3003 	ldr.w	r3, [sl, r3]
 43a:	4806      	ldr	r0, [pc, #24]	; (454 <_start+0x44>)
 43c:	f85a 0000 	ldr.w	r0, [sl, r0]
 440:	f7ff efca 	blx	3d8 <__libc_start_main@plt>
 444:	f7ff efd4 	blx	3f0 <abort@plt>
 448:	00010b7c 	.word	0x00010b7c
 44c:	00000020 	.word	0x00000020
 450:	00000030 	.word	0x00000030
 454:	00000034 	.word	0x00000034

00000458 <call_weak_fn>:
 458:	e59f3014 	ldr	r3, [pc, #20]	; 474 <call_weak_fn+0x1c>
 45c:	e59f2014 	ldr	r2, [pc, #20]	; 478 <call_weak_fn+0x20>
 460:	e08f3003 	add	r3, pc, r3
 464:	e7932002 	ldr	r2, [r3, r2]
 468:	e3520000 	cmp	r2, #0
 46c:	012fff1e 	bxeq	lr
 470:	eaffffdb 	b	3e4 <__gmon_start__@plt>
 474:	00010b5c 	.word	0x00010b5c
 478:	0000002c 	.word	0x0000002c

0000047c <deregister_tm_clones>:
 47c:	4806      	ldr	r0, [pc, #24]	; (498 <deregister_tm_clones+0x1c>)
 47e:	4b07      	ldr	r3, [pc, #28]	; (49c <deregister_tm_clones+0x20>)
 480:	4478      	add	r0, pc
 482:	4a07      	ldr	r2, [pc, #28]	; (4a0 <deregister_tm_clones+0x24>)
 484:	447b      	add	r3, pc
 486:	4283      	cmp	r3, r0
 488:	447a      	add	r2, pc
 48a:	d003      	beq.n	494 <deregister_tm_clones+0x18>
 48c:	4b05      	ldr	r3, [pc, #20]	; (4a4 <deregister_tm_clones+0x28>)
 48e:	58d3      	ldr	r3, [r2, r3]
 490:	b103      	cbz	r3, 494 <deregister_tm_clones+0x18>
 492:	4718      	bx	r3
 494:	4770      	bx	lr
 496:	bf00      	nop
 498:	00010b84 	.word	0x00010b84
 49c:	00010b80 	.word	0x00010b80
 4a0:	00010b38 	.word	0x00010b38
 4a4:	00000028 	.word	0x00000028

000004a8 <register_tm_clones>:
 4a8:	4808      	ldr	r0, [pc, #32]	; (4cc <register_tm_clones+0x24>)
 4aa:	4b09      	ldr	r3, [pc, #36]	; (4d0 <register_tm_clones+0x28>)
 4ac:	4478      	add	r0, pc
 4ae:	4a09      	ldr	r2, [pc, #36]	; (4d4 <register_tm_clones+0x2c>)
 4b0:	447b      	add	r3, pc
 4b2:	1a19      	subs	r1, r3, r0
 4b4:	447a      	add	r2, pc
 4b6:	1089      	asrs	r1, r1, #2
 4b8:	eb01 71d1 	add.w	r1, r1, r1, lsr #31
 4bc:	1049      	asrs	r1, r1, #1
 4be:	d003      	beq.n	4c8 <register_tm_clones+0x20>
 4c0:	4b05      	ldr	r3, [pc, #20]	; (4d8 <register_tm_clones+0x30>)
 4c2:	58d3      	ldr	r3, [r2, r3]
 4c4:	b103      	cbz	r3, 4c8 <register_tm_clones+0x20>
 4c6:	4718      	bx	r3
 4c8:	4770      	bx	lr
 4ca:	bf00      	nop
 4cc:	00010b58 	.word	0x00010b58
 4d0:	00010b54 	.word	0x00010b54
 4d4:	00010b0c 	.word	0x00010b0c
 4d8:	00000038 	.word	0x00000038

000004dc <__do_global_dtors_aux>:
 4dc:	b508      	push	{r3, lr}
 4de:	4b0a      	ldr	r3, [pc, #40]	; (508 <__do_global_dtors_aux+0x2c>)
 4e0:	4a0a      	ldr	r2, [pc, #40]	; (50c <__do_global_dtors_aux+0x30>)
 4e2:	447b      	add	r3, pc
 4e4:	447a      	add	r2, pc
 4e6:	781b      	ldrb	r3, [r3, #0]
 4e8:	b96b      	cbnz	r3, 506 <__do_global_dtors_aux+0x2a>
 4ea:	4b09      	ldr	r3, [pc, #36]	; (510 <__do_global_dtors_aux+0x34>)
 4ec:	58d3      	ldr	r3, [r2, r3]
 4ee:	b123      	cbz	r3, 4fa <__do_global_dtors_aux+0x1e>
 4f0:	4b08      	ldr	r3, [pc, #32]	; (514 <__do_global_dtors_aux+0x38>)
 4f2:	447b      	add	r3, pc
 4f4:	6818      	ldr	r0, [r3, #0]
 4f6:	f7ff ef64 	blx	3c0 <__cxa_finalize@plt>
 4fa:	f7ff ffbf 	bl	47c <deregister_tm_clones>
 4fe:	4b06      	ldr	r3, [pc, #24]	; (518 <__do_global_dtors_aux+0x3c>)
 500:	2201      	movs	r2, #1
 502:	447b      	add	r3, pc
 504:	701a      	strb	r2, [r3, #0]
 506:	bd08      	pop	{r3, pc}
 508:	00010b22 	.word	0x00010b22
 50c:	00010adc 	.word	0x00010adc
 510:	00000024 	.word	0x00000024
 514:	00010b0e 	.word	0x00010b0e
 518:	00010b02 	.word	0x00010b02

0000051c <frame_dummy>:
 51c:	e7c4      	b.n	4a8 <register_tm_clones>
 51e:	bf00      	nop

00000520 <__libc_csu_init>:
 520:	e92d 43f8 	stmdb	sp!, {r3, r4, r5, r6, r7, r8, r9, lr}
 524:	4607      	mov	r7, r0
 526:	4e0c      	ldr	r6, [pc, #48]	; (558 <__libc_csu_init+0x38>)
 528:	4688      	mov	r8, r1
 52a:	4d0c      	ldr	r5, [pc, #48]	; (55c <__libc_csu_init+0x3c>)
 52c:	4691      	mov	r9, r2
 52e:	447e      	add	r6, pc
 530:	f7ff ef36 	blx	3a0 <_init>
 534:	447d      	add	r5, pc
 536:	1b76      	subs	r6, r6, r5
 538:	10b6      	asrs	r6, r6, #2
 53a:	d00a      	beq.n	552 <__libc_csu_init+0x32>
 53c:	3d04      	subs	r5, #4
 53e:	2400      	movs	r4, #0
 540:	3401      	adds	r4, #1
 542:	f855 3f04 	ldr.w	r3, [r5, #4]!
 546:	464a      	mov	r2, r9
 548:	4641      	mov	r1, r8
 54a:	4638      	mov	r0, r7
 54c:	4798      	blx	r3
 54e:	42a6      	cmp	r6, r4
 550:	d1f6      	bne.n	540 <__libc_csu_init+0x20>
 552:	e8bd 83f8 	ldmia.w	sp!, {r3, r4, r5, r6, r7, r8, r9, pc}
 556:	bf00      	nop
 558:	00010996 	.word	0x00010996
 55c:	0001098c 	.word	0x0001098c

00000560 <__libc_csu_fini>:
 560:	4770      	bx	lr
 562:	bf00      	nop

Disassembly of section .fini:

00000564 <_fini>:
 564:	e92d4008 	push	{r3, lr}
 568:	e8bd8008 	pop	{r3, pc}
