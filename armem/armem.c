#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include "armem.h"


//
// read the entire contents of a file into a dynamically allocated buffer.
// parameters:
//   const char *path     - the file to read.
//   size_t     *exe_size - will be set to the size of the data read.
// returns:
//   void * - the data read. This is a dynamically allocated buffer and
//            must be freed by the caller.
//

void *read_file(const char *path, size_t *exe_size)
{
    // Open the file.
    int fd = open(path, 0);
    if (fd < 0)
    {
        fprintf(stderr, "can't open '%s': %s\n", path, strerror(errno));
        exit(EXIT_FAILURE);
    }

    // Get the size.
    struct stat finfo;
    if (fstat(fd, &finfo))
    {
        fprintf(stderr, "can't stat '%s': %s\n", path, strerror(errno));
        exit(EXIT_FAILURE);
    }

    // Read the file.
    void *exe_data = calloc(finfo.st_size, 1);
    if (read(fd, exe_data, finfo.st_size) < finfo.st_size)
    {
        free(exe_data);
        fprintf(stderr, "can't read '%s': %s\n", path, strerror(errno));
        exit(EXIT_FAILURE);
    }

    *exe_size = finfo.st_size;

    return exe_data;
}


//
// the main program.
//

int main(int argc, char **argv)
{
    // Check args.
    if (argc != 2)
    {
        fprintf(stderr, "Format: armem <exe>\n");
        return EXIT_FAILURE;
    }

    // Read the source file.
    size_t exe_size;
    void *exe_data = read_file(argv[1], &exe_size);

    // Parse the ELF data from the executable.
    armem_sections_t sections;
    if (!armem_parse_elf(&sections, exe_data, exe_size))
        return EXIT_FAILURE;

    // Clean up.
    free(exe_data);

    return EXIT_SUCCESS;
}