#pragma once

#include <inttypes.h>

// Operating system ABI.
#define ABI_SYSTEM_V       0x00
#define ABI_HP_UX          0x01
#define ABI_NETBSD         0x02
#define ABI_LINUX          0x03
#define ABI_GNU_HURD       0x04
#define ABI_SOLARIS        0x06
#define ABI_AIX            0x07
#define ABI_IRIX           0x08
#define ABI_FREEBSD        0x09
#define ABI_TRU64          0x0A
#define ABI_NOVELL_MODESTO 0x0B
#define ABI_OPENBSD        0x0C
#define ABI_OPENVMS        0x0D
#define ABI_NONSTOP_KERNEL 0x0E
#define ABI_AROS           0x0F
#define ABI_FENIX_OS       0x10
#define ABI_CLOUDABI       0x11


// Object file types
#define ET_NONE            0x0000
#define ET_REL             0x0001
#define ET_EXEC            0x0002
#define ET_DYN             0x0003
#define ET_CORE            0x0004
#define ET_LOOS            0xfe00
#define ET_HIOS            0xfeff
#define ET_LOPROC          0xff00
#define ET_HIPROC          0xffff


// Target instruction set architecture.
#define ISA_NONE           0x00
#define ISA_SPARC          0x02
#define ISA_X86            0x03
#define ISA_MIPS           0x08
#define ISA_POWERPC        0x14
#define ISA_S390           0x16
#define ISA_ARM            0x28
#define ISA_SUPERH         0x2A
#define ISA_IA_64          0x32
#define ISA_X86_64         0x3E
#define ISA_AARCH64        0xB7
#define ISA_RISC_V         0xF3


// Section types.
#define SHT_NULL           0x00          // Section header table entry unused
#define SHT_PROGBITS       0x01          // Program data
#define SHT_SYMTAB         0x02          // Symbol table
#define SHT_STRTAB         0x03          // String table
#define SHT_RELA           0x04          // Relocation entries with addends
#define SHT_HASH           0x05          // Symbol hash table
#define SHT_DYNAMIC        0x06          // Dynamic linking information
#define SHT_NOTE           0x07          // Notes
#define SHT_NOBITS         0x08          // Program space with no data (bss)
#define SHT_REL            0x09          // Relocation entries, no addends
#define SHT_SHLIB          0x0A          // Reserved
#define SHT_DYNSYM         0x0B          // Dynamic linker symbol table
#define SHT_INIT_ARRAY     0x0E          // Array of constructors
#define SHT_FINI_ARRAY     0x0F          // Array of destructors
#define SHT_PREINIT_ARRAY  0x10          // Array of pre-constructors
#define SHT_GROUP          0x11          // Section group
#define SHT_SYMTAB_SHNDX   0x12          // Extended section indices
#define SHT_NUM            0x13          // Number of defined types.
#define SHT_LOOS           0x60000000    // Start OS-specific.


// Program header types.
#define PT_NULL            0x00          // Program header table entry unused
#define PT_LOAD            0x01          // Loadable segment
#define PT_DYNAMIC         0x02          // Dynamic linking information
#define PT_INTERP          0x03          // Interpreter information
#define PT_NOTE            0x04          // Auxiliary information
#define PT_SHLIB           0x05          // reserved
#define PT_PHDR            0x06          // segment containing program header table itself


// Dynamic reloc tags.
#define DT_NULL            0 	         // ignored
#define DT_NEEDED          1 	         // d_val 
#define DT_PLTRELSZ        2 	         // d_val 
#define DT_PLTGOT          3 	         // d_ptr 
#define DT_HASH            4 	         // d_ptr 
#define DT_STRTAB          5 	         // d_ptr 
#define DT_SYMTAB          6 	         // d_ptr 
#define DT_RELA            7 	         // d_ptr 
#define DT_RELASZ          8 	         // d_val 
#define DT_RELAENT         9 	         // d_val 
#define DT_STRSZ           10 	         // d_val 
#define DT_SYMENT          11 	         // d_val 
#define DT_INIT            12 	         // d_ptr 
#define DT_FINI            13 	         // d_ptr 
#define DT_SONAME          14 	         // d_val 
#define DT_RPATH           15 	         // d_val 
#define DT_SYMBOLIC        16 	         // ignored
#define DT_REL             17 	         // d_ptr 
#define DT_RELSZ           18 	         // d_val 
#define DT_RELENT          19 	         // d_val 
#define DT_PLTREL          20 	         // d_val 
#define DT_DEBUG           21 	         // d_ptr 
#define DT_TEXTREL         22 	         // ignored
#define DT_JMPREL          23 	         // d_ptr 
#define DT_BIND_NOW        24 	         // ignored
#define DT_INIT_ARRAY      25 	         // d_ptr 
#define DT_FINI_ARRAY      26 	         // d_ptr 
#define DT_INIT_ARRAYSZ    27 	         // d_val 
#define DT_FINI_ARRAYSZ    28 	         // d_val 
#define DT_RUNPATH         29 	         // d_val 
#define DT_FLAGS           30 	         // d_val 
#define DT_ENCODING        31 	         // unspecied
#define DT_PREINIT_ARRAY   32 	         // d_ptr 
#define DT_PREINIT_ARRAYSZ 33 	         // d_val 


//
// ELF file header (packed).
//

typedef struct 
{
    uint8_t  e_magic[4];    // 0x7F followed by ELF(45 4c 46) in ASCII; these four bytes constitute the magic number. 
    uint8_t  e_class;       // This byte is set to either 1 or 2 to signify 32- or 64-bit format, respectively. 
    uint8_t  e_endian;      // This byte is set to either 1 or 2 to signify little or big endianness, respectively. This affects interpretation of multi-byte fields starting with offset 0x10. 
    uint8_t  e_version1;    // Set to 1 for the original and current version of ELF. 
    uint8_t  e_osabi;       // Identifies the target operating system ABI.
                            //   0x00  System V
                            //   0x01  HP-UX
                            //   0x02  NetBSD
                            //   0x03  Linux
                            //   0x04  GNU Hurd
                            //   0x06  Solaris
                            //   0x07  AIX
                            //   0x08  IRIX
                            //   0x09  FreeBSD
                            //   0x0A  Tru64
                            //   0x0B  Novell Modesto
                            //   0x0C  OpenBSD
                            //   0x0D  OpenVMS
                            //   0x0E  NonStop Kernel
                            //   0x0F  AROS
                            //   0x10  Fenix OS
                            //   0x11  CloudABI    
    uint8_t  e_abiversion;  // Further specifies the ABI version. Its interpretation depends on the target ABI. Currently unused by Linux.
    uint8_t  e_padding[7];  // Unused.
    uint16_t e_type;        // Identifies object file type. 
                            //   0x0000  ET_NONE
                            //   0x0001  ET_REL
                            //   0x0002  ET_EXEC
                            //   0x0003  ET_DYN
                            //   0x0004  ET_CORE
                            //   0xfe00  ET_LOOS
                            //   0xfeff  ET_HIOS
                            //   0xff00  ET_LOPROC
                            //   0xffff  ET_HIPROC    
    uint16_t e_machine;     // Specifies target instruction set architecture.
                            //   0x00  No specific instruction set
                            //   0x02  SPARC
                            //   0x03  x86
                            //   0x08  MIPS
                            //   0x14  PowerPC
                            //   0x16  S390
                            //   0x28  ARM
                            //   0x2A  SuperH
                            //   0x32  IA-64
                            //   0x3E  x86-64
                            //   0xB7  AArch64
                            //   0xF3  RISC-V
    uint32_t e_version2;    // Set to 1 for the original version of ELF. 
    uint32_t e_entry;       // This is the memory address of the entry point from where the process starts executing. This field is either 32 or 64 bits long depending on the format defined earlier. 
    uint32_t e_phoff;       // Points to the start of the program header table. It usually follows the file header immediately, making the offset 0x34 or 0x40 for 32- and 64-bit ELF executables, respectively. 
    uint32_t e_shoff;       // Points to the start of the section header table. 
    uint32_t e_flags;       // Interpretation of this field depends on the target architecture. 
    uint16_t e_ehsize;      // Contains the size of this header, normally 64 Bytes for 64-bit and 52 Bytes for 32-bit format. 
    uint16_t e_phentsize;   // Contains the size of a program header table entry. 
    uint16_t e_phnum;       // Contains the number of entries in the program header table.
    uint16_t e_shentsize;   // Contains the size of a section header table entry. 
    uint16_t e_shnum;       // Contains the number of entries in the section header table. 
    uint16_t e_shstrndx;    // Contains index of the section header table entry that contains the section names.

} __attribute__((packed)) elf_file_header_t;


//
// ELF program header (packed).
//

typedef struct
{
    uint32_t p_type;        // Identifies the type of the segment. 
                            //   0x00000000     PT_NULL     Program header table entry unused
                            //   0x00000001     PT_LOAD     Loadable segment
                            //   0x00000002     PT_DYNAMIC  Dynamic linking information
                            //   0x00000003     PT_INTERP   Interpreter information
                            //   0x00000004     PT_NOTE     Auxiliary information
                            //   0x00000005     PT_SHLIB    reserved
                            //   0x00000006     PT_PHDR     segment containing program header table itself
    uint32_t p_offset;      // Offset of the segment in the file image. 
    uint32_t p_vaddr;       // Virtual address of the segment in memory.
    uint32_t p_paddr;       // On systems where physical address is relevant, reserved for segment's physical address.
    uint32_t p_filesz;      // Size in bytes of the segment in the file image. May be 0.
    uint32_t p_memsz;       // Size in bytes of the segment in memory. May be 0.
    uint32_t p_flags;       // Segment-dependent flags (position for 32-bit structure).
    uint32_t p_align;       // 0 and 1 specify no alignment. Otherwise should be a positive, integral power of 2, with p_vaddr equating p_offset modulus p_align.
} __attribute__((packed)) elf_program_header_t;


//
// ELF section header (packed).
//

typedef struct
{
    uint32_t sh_name;       // An offset to a string in the .shstrtab section that represents the name of this section. 
    uint32_t sh_type;       // Identifies the type of this header.
                            //   0x00000000    SHT_NULL           Section header table entry unused
                            //   0x00000001    SHT_PROGBITS       Program data
                            //   0x00000002    SHT_SYMTAB         Symbol table
                            //   0x00000003    SHT_STRTAB         String table
                            //   0x00000004    SHT_RELA           Relocation entries with addends
                            //   0x00000005    SHT_HASH           Symbol hash table
                            //   0x00000006    SHT_DYNAMIC        Dynamic linking information
                            //   0x00000007    SHT_NOTE           Notes
                            //   0x00000008    SHT_NOBITS         Program space with no data (bss)
                            //   0x00000009    SHT_REL            Relocation entries, no addends
                            //   0x0000000A    SHT_SHLIB          Reserved
                            //   0x0000000B    SHT_DYNSYM         Dynamic linker symbol table
                            //   0x0000000E    SHT_INIT_ARRAY     Array of constructors
                            //   0x0000000F    SHT_FINI_ARRAY     Array of destructors
                            //   0x00000010    SHT_PREINIT_ARRAY  Array of pre-constructors
                            //   0x00000011    SHT_GROUP          Section group
                            //   0x00000012    SHT_SYMTAB_SHNDX   Extended section indices
                            //   0x00000013    SHT_NUM            Number of defined types.
                            //   0x60000000    SHT_LOOS           Start OS-specific.
    uint32_t sh_flags;      // Identifies the attributes of the section. 
                            //   0x00000001    SHF_WRITE          Writable
                            //   0x00000002    SHF_ALLOC          Occupies memory during execution
                            //   0x00000004    SHF_EXECINSTR      Executable
                            //   0x00000010    SHF_MERGE          Might be merged
                            //   0x00000020    SHF_STRINGS        Contains nul-terminated strings
                            //   0x00000040    SHF_INFO_LINK      'sh_info' contains SHT index
                            //   0x00000080    SHF_LINK_ORDER     Preserve order after combining
                            //   0x00000100    SHF_OS_NONCONFORMING  Non-standard OS specific handling required
                            //   0x00000200    SHF_GROUP          Section is member of a group
                            //   0x00000400    SHF_TLS            Section hold thread-local data
                            //   0x0ff00000    SHF_MASKOS         OS-specific
                            //   0xf0000000    SHF_MASKPROC       Processor-specific
                            //   0x04000000    SHF_ORDERED        Special ordering requirement (Solaris)
                            //   0x08000000    SHF_EXCLUDE        Section is excluded unless referenced or allocated (Solaris) 
    uint32_t sh_addr;       // Virtual address of the section in memory, for sections that are loaded.
    uint32_t sh_offset;     // Offset of the section in the file image.
    uint32_t sh_size;       // Size in bytes of the section in the file image. May be 0.
    uint32_t sh_link;       // Contains the section index of an associated section. This field is used for several purposes, depending on the type of section.
    uint32_t sh_info;       // Contains extra information about the section. This field is used for several purposes, depending on the type of section.
    uint32_t sh_addralign;  // Contains the required alignment of the section. This field must be a power of two.
    uint32_t sh_entsize;    // Contains the size, in bytes, of each entry, for sections that contain fixed-size entries. Otherwise, this field contains zero.

} __attribute__((packed)) elf_section_header_t;


//
// Symbol table.
//

typedef struct {
	uint32_t st_name;       // Symbol table index of the name.
	uint32_t st_value;      // Value of the symbol.
	uint32_t st_size;       // Size in bytes.
	uint8_t  st_info;       // Symbol type and binding attributes.
	uint8_t  st_other;      // Symbol is visible if non-zero.
	uint16_t st_shndx;      // Index of this symbol.
} __attribute__((packed)) elf_symbol_t;


//
// Dynamic relocation table.
//

typedef struct {
	int32_t d_tag;
   	union {
   		uint32_t    d_val;
   		uint32_t    d_ptr;
	} d_un;
} __attribute__((packed)) elf_dynamic_reloc_t;


#define ELF32_ST_BIND(i)   ((i)>>4)
#define ELF32_ST_TYPE(i)   ((i)&0xf)
