#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <memory.h>
#include <inttypes.h>

#include "armem.h"
#include "elf.h"


#define DUMP_LINE_LEN      32

// These correspond to the values of elf_program_header_t->p_type.
static char *phdr_type_strs[] = 
{
    "PT_NULL",
    "PT_LOAD",
    "PT_DYNAMIC",
    "PT_INTERP",
    "PT_NOTE",
    "PT_SHLIB",
    "PT_PHDR"
};

// Names corresponding to the possible values of shdr->sh_type.
static const char *shdr_type_strs[] =
{
    "SHT_NULL",
    "SHT_PROGBITS",
    "SHT_SYMTAB",
    "SHT_STRTAB",
    "SHT_RELA",
    "SHT_HASH",
    "SHT_DYNAMIC",
    "SHT_NOTE",
    "SHT_NOBITS",
    "SHT_REL",
    "SHT_SHLIB",
    "SHT_DYNSYM",
    "SHT_INIT_ARRAY",
    "SHT_FINI_ARRAY",
    "SHT_PREINIT_ARRAY",
    "SHT_GROUP",
    "SHT_SYMTAB_SHNDX",
    "SHT_NUM"
};

// Names corresponding to the bit field values of shdr->sh_flags.
static const char *shdr_flags_strs[] = 
{
    "SHF_WRITE",            // 0x0001
    "SHF_ALLOC",            // 0x0002
    "SHF_EXECINSTR",        // 0x0004
    "",                     // 0x0008
    "SHF_MERGE",            // 0x0010
    "SHF_STRINGS",          // 0x0020
    "SHF_INFO_LINK",        // 0x0040
    "SHF_LINK_ORDER",       // 0x0080
    "SHF_OS_NONCONFORMING", // 0x0100
    "SHF_GROUP",            // 0x0200
    "SHF_TLS"               // 0x0400
};
   
// Names corresponding to the values of ELF32_ST_BIND(symtab->sh_info).
static const char *symtab_bind_strs[] = 
{
    "LOCAL",                // 0
    "GLOBAL",               // 1
    "WEAK",                 // 2
    NULL,                   // 3
    NULL,                   // 4
    NULL,                   // 5
    NULL,                   // 6
    NULL,                   // 7
    NULL,                   // 8
    NULL,                   // 9
    "LOOS",                 // 10
    NULL,                   // 11
    "HIOS",                 // 12
    "LOPROC",               // 13
    NULL,                   // 14
    "HIPROC"                // 15
};

// Names corresponding to the values of ELF32_ST_TYPE(symtab->sh_info).
static const char *symtab_type_strs[] = 
{
    "NOTYPE",               // 0
    "OBJECT",               // 1
    "FUNC",                 // 2
    "SECTION",              // 3
    "FILE",                 // 4
    "COMMON",               // 5
    "TLS",                  // 6
    NULL,                   // 7
    NULL,                   // 8
    NULL,                   // 9
    "LOOS",                 // 10
    NULL,                   // 11
    "HIOS",                 // 12
    "LOPROC",               // 13
    NULL,                   // 14
    "HIPROC"                // 15
};

// Names corresponding to the values of symtab->sh_other & 0x3.
static const char *symtab_vis_strs[] = 
{
    "DEFAULT",              // 0
    "INTERNAL",             // 1
    "HIDDEN",               // 2
    "PROTECTED",            // 3
};


// Parse an ELF executable.
// Returns false on error.
bool armem_parse_elf(armem_sections_t *sections, void *buf, size_t len)
{
    static uint8_t magic[4] = { 0x7f, 'E', 'L', 'F' };
    elf_file_header_t *fhdr = buf;
    elf_symbol_t *symtab = NULL;
    size_t        symtab_entries = 0;
    char         *strtab = NULL;
    size_t        strtab_size = 0;

    memset(sections, 0, sizeof(*sections));

    // Check validity of the file header.
    if (len < sizeof(elf_file_header_t) || 
        memcmp(fhdr->e_magic, magic, sizeof(magic)) != 0)
    {
        fprintf(stderr, "not a valid ELF file\n");
        return false;
    }

    if (fhdr->e_class != 1)
    {
        fprintf(stderr, "not a 32 bit ELF file\n");
        return false;
    }

    if (fhdr->e_endian != 1)
    {
        fprintf(stderr, "not a little endian ELF file\n");
        return false;
    }

    if (fhdr->e_version1 != 1 || fhdr->e_version2 != 1)
    {
        fprintf(stderr, "not a supported ELF file format\n");
        return false;
    }

    if (fhdr->e_osabi != ABI_SYSTEM_V && fhdr->e_osabi != ABI_LINUX)
    {
        fprintf(stderr, "not a Linux ELF file format\n");
        return false;
    }

    if (fhdr->e_type != ET_EXEC && fhdr->e_type != ET_DYN)
    {
        fprintf(stderr, "not an executable ELF file format\n");
        return false;
    }

    if (fhdr->e_machine != ISA_ARM)
    {
        fprintf(stderr, "not an ARM ELF file format\n");
        return false;
    }

    // Check the program header table.
    if (len < fhdr->e_phoff + fhdr->e_phentsize * fhdr->e_phnum ||
        fhdr->e_phentsize != sizeof(elf_program_header_t))
    {
        fprintf(stderr, "not a valid ELF file - invalid program header table\n");
        return false;
    }

    // Go through the program header table.
    printf("program header table\n");
    for (size_t i = 0; i < fhdr->e_phnum; i++)
    {
        elf_program_header_t *phdr = (elf_program_header_t *)(buf + fhdr->e_phoff + sizeof(elf_program_header_t) * i);
        char type_str[20];

        memset(type_str, 0, sizeof(type_str));
        
        if (phdr->p_type < sizeof(phdr_type_strs)/sizeof(phdr_type_strs[0]))
        {
            strncpy(type_str, phdr_type_strs[phdr->p_type], sizeof(type_str)-1);
        }
        else
        {
            sprintf(type_str, "%08x", phdr->p_type);
        }

        printf("%2zu: %-10s %8x %8x %8x %8x %8x %3x %6x\n", 
            i, 
            type_str, phdr->p_offset, phdr->p_vaddr, phdr->p_paddr, 
            phdr->p_filesz, phdr->p_memsz, phdr->p_flags, phdr->p_align);
    }

    // Check the section names.
    if (fhdr->e_shstrndx >= fhdr->e_shnum)
    {
        fprintf(stderr, "not a valid ELF file - invalid section names\n");
        return false;
    }
    
    elf_section_header_t *str_shdr = (elf_section_header_t *)(buf + fhdr->e_shoff + sizeof(elf_section_header_t) * fhdr->e_shstrndx);

    // Check the section header table.
    if (len < fhdr->e_shoff + fhdr->e_shentsize * fhdr->e_shnum ||
        fhdr->e_shentsize != sizeof(elf_section_header_t))
    {
        fprintf(stderr, "not a valid ELF file - invalid section header table\n");
        return false;
    }

    // Go through the section header table.
    printf("\nsection header table\n");
    for (size_t i = 0; i < fhdr->e_shnum; i++)
    {
        elf_section_header_t *shdr = (elf_section_header_t *)(buf + fhdr->e_shoff + sizeof(elf_section_header_t) * i);
        char type_str[20];
        char flags_str[256];

        // Get the section name.
        if (str_shdr->sh_offset >= len ||
            shdr->sh_name >= str_shdr->sh_size)
        {
            fprintf(stderr, "not a valid ELF file - invalid section name offset\n");
            return false;
        }

        char *name = buf + str_shdr->sh_offset + shdr->sh_name;
        
        // Get the type name.
        memset(type_str, 0, sizeof(type_str));
        
        if (shdr->sh_type < sizeof(shdr_type_strs)/sizeof(shdr_type_strs[0]))
        {
            strncpy(type_str, shdr_type_strs[shdr->sh_type], sizeof(type_str)-1);
        }
        else
        {
            sprintf(type_str, "%x", shdr->sh_type);
        }

        // Get the flags as a string.
        memset(flags_str, 0, sizeof(flags_str));
        bool extra_flags = shdr->sh_flags == 0;

        for (size_t j = 0; j < sizeof(shdr->sh_flags) * 8; j++)
        {
            // If this flag bit is set and the flag string isn't empty.
            if (shdr->sh_flags & (1<<j))
            {
                if (shdr_flags_strs[j][0] && j < sizeof(shdr_flags_strs)/sizeof(shdr_flags_strs[0]))
                {
                    // Add a separator '|' if necessary.
                    if (flags_str[0])
                    {
                        strcat(flags_str, " | ");
                    }

                    // Append the flag string.
                    strcat(flags_str, shdr_flags_strs[j]);
                }
                else
                {
                    // Otherwise just note that we need to mention the hex.
                    extra_flags = true;
                }
            }
        }

        if (extra_flags)
        {
            // Add a separator '|' if necessary.
            if (flags_str[0])
            {
                strcat(flags_str, " | ");
            }

            // Append the flag string.
           sprintf(flags_str + strlen(flags_str), "%x", shdr->sh_flags);
        }

        printf("%2zu: %-18s %-18s %-27s %8x %4x %4x %4x %4x %x %2x\n", 
            i, name, type_str, flags_str,
            shdr->sh_addr, 
            shdr->sh_offset, shdr->sh_size, shdr->sh_link, shdr->sh_info,
            shdr->sh_addralign, shdr->sh_entsize);

        if (strcmp(name, ".text") == 0 || strcmp(name, ".data") == 0 || strcmp(name, ".rodata") == 0 || strcmp(name, ".bss") == 0 || strcmp(name, ".symtab") == 0)
        {
            // Hex dump the section contents.
            for (uint32_t off = 0; off < shdr->sh_size; off += DUMP_LINE_LEN)
            {
                printf("    %08x:", off);
                for (uint32_t loff = 0; loff < DUMP_LINE_LEN && loff < shdr->sh_size - off; loff++)
                {
                    printf(" %02x", *((uint8_t *)buf + shdr->sh_offset + off + loff));
                }

                printf("\n");
            }
        }

        // Get the sections we're interested in.
        if (strcmp(name, ".text") == 0 && shdr->sh_type == SHT_PROGBITS)
        {
            sections->text = (uint8_t *)buf + shdr->sh_offset;
            sections->text_size = shdr->sh_size;
        }
        else if (strcmp(name, ".data") == 0 && shdr->sh_type == SHT_PROGBITS)
        {
            sections->data = (uint8_t *)buf + shdr->sh_offset;
            sections->data_size = shdr->sh_size;
        }
        else if (strcmp(name, ".rodata") == 0 && shdr->sh_type == SHT_PROGBITS)
        {
            sections->rodata = (uint8_t *)buf + shdr->sh_offset;
            sections->rodata_size = shdr->sh_size;
        }
        else if (strcmp(name, ".bss") == 0 && shdr->sh_type == SHT_NOBITS)
        {
            sections->bss = (uint8_t *)buf + shdr->sh_offset;
            sections->bss_size = shdr->sh_size;
        }
        else if (strcmp(name, ".symtab") == 0 && shdr->sh_type == SHT_SYMTAB)
        {
            symtab = (elf_symbol_t *)(buf + shdr->sh_offset);
            symtab_entries = shdr->sh_size / shdr->sh_entsize;
        }
        else if (strcmp(name, ".strtab") == 0 && shdr->sh_type == SHT_STRTAB)
        {
            strtab = (char *)(buf + shdr->sh_offset);
            strtab_size = shdr->sh_size;
        }
        else if (strcmp(name, ".dynamic") == 0 && shdr->sh_type == SHT_DYNAMIC)
        {
            sections->dynamic = (uint8_t *)buf + shdr->sh_offset;
            sections->dynamic_size = shdr->sh_size;
        }
        else if (strcmp(name, ".hash") == 0 && shdr->sh_type == SHT_HASH)
        {
            sections->hash = (uint8_t *)buf + shdr->sh_offset;
            sections->hash_size = shdr->sh_size;
        }
        else if (strcmp(name, ".got") == 0 && shdr->sh_type == SHT_PROGBITS)
        {
            sections->got = (uint8_t *)buf + shdr->sh_offset;
            sections->got_size = shdr->sh_size;
        }
        else if (strcmp(name, ".plt") == 0 && shdr->sh_type == SHT_PROGBITS)
        {
            sections->plt = (uint8_t *)buf + shdr->sh_offset;
            sections->plt_size = shdr->sh_size;
        }
        
    }

    // Show the symbol table.
    if (symtab)
    {
        printf("symbol table:\n");
        for (size_t i = 0; i < symtab_entries; i++)
        {
            elf_symbol_t *sym = &symtab[i];
            char *name = strtab + sym->st_name;
            uint8_t sym_bind = ELF32_ST_BIND(sym->st_info);
            uint8_t sym_type = ELF32_ST_TYPE(sym->st_info);
            uint8_t sym_vis  = sym->st_other & 0x3;
            char *bind = symtab_bind_strs[sym_bind] ? symtab_bind_strs[sym_bind] : "-";
            char *typ  = symtab_type_strs[sym_type] ? symtab_type_strs[sym_type] : "-";
            
            printf("%6u: %08x %4x %7s %7s %9s %4x %s\n", i, sym->st_value, sym->st_size, typ, bind, symtab_vis_strs[sym_vis], sym->st_shndx, name);
        }

        printf("\n");
    }

    // Show the _DYNAMIC section.
    if (sections->dynamic)
    {
        elf_dynamic_reloc_t *dyntab = (elf_dynamic_reloc_t *)sections->dynamic;
        size_t dynamic_entries = sections->dynamic_size / sizeof(elf_dynamic_reloc_t);
        printf("dynamic relocations:\n");
        for (size_t i = 0; i < dynamic_entries; i++)
        {
            elf_dynamic_reloc_t *reloc = &dyntab[i];

            // switch (reloc->d_tag)
            // {
            //     case 
            // }

            printf("%6d: %8x %8x %8x\n", i, reloc->d_tag, reloc->d_un.d_val, reloc->d_un.d_ptr);
        }

        printf("\n");
    }

    return true;
}
