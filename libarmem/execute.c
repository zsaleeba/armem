#include "armem.h"

#pragma packed(push)

struct arm_thumb_top_t
{
    unsigned other    :  9;
    unsigned opcode   :  7;
};

struct arm_thumb_dp_t
{
    unsigned other    :  6;
    unsigned dpop     :  4;
    unsigned opcode   :  6;
};

struct arm_instr_swi_t
{
    unsigned offset   : 24;
    unsigned opsel    :  4;
    unsigned cond     :  4;
};

struct arm_instr_dp_t
{
    unsigned operand2 : 12;
    unsigned rd       :  4;
    unsigned rn       :  4;
    unsigned s        :  1;
    unsigned opcode   :  4;
    unsigned i        :  1;
    unsigned opsel    :  2;
    unsigned cond     :  4;
}

#pragma packed(pop)

void reset_cpu(armem_cpu_state_t *cpu)
{
    memset(cpu, 0, sizeof(cpu));
}

static execute_instruction32(cpu_state_t *cpu)
{
    exception_unhandled_instruction(cpu);
}

void execute_instruction(cpu_state_t *cpu)
{
    uint16_t instr = mem_read16(cpu, cpu->r[REG_PC]);
    uint8_t  opcode = ((arm_thumb_top_t *)&instr)->opcode;

    switch (opcode)
    {
        case 0x00:
        case 0x01:
        case 0x02:
        case 0x03:
            // LSL.
            break;

        case 0x04:
        case 0x05:
        case 0x06:
        case 0x07:
            // LSR.
            break;

        case 0x08:
        case 0x09:
        case 0x0a:
        case 0x0b:
            // ASR.
            break;

        case 0x0c:
            // ADD register.
            break;

        case 0x0d:
            // SUB register.
            break;

        case 0x0e:
            // ADD immediate.
            break;

        case 0x0f:
            // SUB immediate.
            break;

        case 0x10:
        case 0x11:
        case 0x12:
        case 0x13:
            // MOV immediate.
            break;

        case 0x14:
        case 0x15:
        case 0x16:
        case 0x17:
            // CMP immediate.
            break;

        case 0x18:
        case 0x19:
        case 0x1a:
        case 0x1b:
            // ADD immediate.
            break;

        case 0x1c:
        case 0x1d:
        case 0x1e:
        case 0x1f:
            // SUB immediate.
            break;

        case 0x20:
        case 0x21:
            // Data processing instruction.
            switch (((arm_thumb_dp_t *)&instr)->opcode)
            {
                case 0x0:
                    // Bitwise AND register.
                    break;

                case 0x1:
                    // Bitwise EOR register.
                    break;
                    
                case 0x2:
                    // LSL register.
                    break;
                    
                case 0x3:
                    // LSR register.
                    break;
                    
                case 0x4:
                    // ASR register.
                    break;
                    
                case 0x5:
                    // ADC register.
                    break;
                    
                case 0x6:
                    // SBC register.
                    break;
                    
                case 0x7:
                    // ROR register.
                    break;
                    
                case 0x8:
                    // TEST register.
                    break;
                    
                case 0x9:
                    // RSB immediate.
                    break;
                    
                case 0xa:
                    // CMP register.
                    break;
                    
                case 0xb:
                    // CMN register.
                    break;
                    
                case 0xc:
                    // ORR register.
                    break;
                    
                case 0xd:
                    // MUL.
                    break;
                    
                case 0xe:
                    // BIC register.
                    break;
                    
                case 0xf:
                    // MVN register.
                    break;
                }
            break;

        case 0x22:
        case 0x23:
            // Data processing instruction.
            switch (((arm_thumb_dp_t *)&instr)->opcode)
            {
                case 0x0:
                    // ADD low registers.
                    break;

                case 0x1:
                case 0x2:
                case 0x3:
                    // ADD high registers.
                    break;
                    
                case 0x4:
                case 0x5:
                case 0x6:
                case 0x7:
                    // CMP high registers.
                    break;
                    
                case 0x8:
                    // MOV low registers.
                    break;
                    
                case 0x9:
                case 0xa:
                case 0xb:
                    // MOV high registers.
                    break;
                    
                case 0xc:
                case 0xd:
                    // BX.
                    break;
                    
                case 0xe:
                case 0xf:
                    // BLX.
                    break;
            }
            break;

        case 0x24:
        case 0x25:
        case 0x26:
        case 0x27:
            // LDR literal.
            break;

        case 0x28:
        case 0x29:
        case 0x2a:
        case 0x2b:
        case 0x2c:
        case 0x2d:
        case 0x2e:
        case 0x2f:
        case 0x30:
        case 0x31:
        case 0x32:
        case 0x33:
        case 0x34:
        case 0x35:
        case 0x36:
        case 0x37:
        case 0x38:
        case 0x39:
        case 0x3a:
        case 0x3b:
        case 0x3c:
        case 0x3d:
        case 0x3e:
        case 0x3f:
        case 0x40:
        case 0x41:
        case 0x42:
        case 0x43:
        case 0x44:
        case 0x45:
        case 0x46:
        case 0x47:
        case 0x48:
        case 0x49:
        case 0x4a:
        case 0x4b:
        case 0x4c:
        case 0x4d:
        case 0x4e:
        case 0x4f:
            // Load/store single data item.
            break;

        case 0x50:
        case 0x51:
        case 0x52:
        case 0x53:
            // ADR.
            break;

        case 0x54:
        case 0x55:
        case 0x56:
        case 0x57:
            // ADD SP plus immediate.
            break;

        case 0x58:
            switch ( (instr >> 7) & 0x3)
            {
                case 0x0:
                    // ADD (SP plus immediate).
                    break;
                
                case 0x1:
                    // SUB (SP minus immediate).
                    break;

                case 0x2:
                case 0x3:
                    // CBZ.
            }
            break;

        case 0x59:
            switch ( (instr >> 6) & 0x7)
            {
                case 0x0:
                    // SXTH.
                    break;
                    
                case 0x1:
                    // SXTB.
                    break;
                    
                case 0x2:
                    // UXTH.
                    break;
                    
                case 0x3:
                    // UXTB.
                    break;

                default:
                    // CBZ, CBNZ.
                    break;
            }
            break;

        case 0x5a:
            // PUSH multiple registers.
            break;

        case 0x5b:
            switch ( (instr >> 5) & 0xf)
            {
                case 0x2:
                    // SETEND.
                    break;

                case 0x3:
                    // CPS.
                    break;

                default:
                    exception_invalid_instruction(cpu);
                    break;
            }
            break;

        case 0x5c:
            // CBZ, CBNZ.
            break;

        case 0x5d:
            switch ( (instr >> 6) & 0x7)
            {
                case 0x0:
                    // REV.
                    break;

                case 0x1:
                    // REV16.
                    break;

                case 0x2:
                    exception_invalid_instruction(cpu);
                    break;

                case 0x3:
                    // REVSH.
                    break;

                case 0x4:
                case 0x5:
                case 0x6:
                case 0x7:
                    // CBZ, CBNZ.
            }
            break;

        case 0x5e:
            // POP multiple registers.
            break;

        case 0x5f:
            if (instr & 0x0800)
            {
                // If-Then and hints.
            }
            else
            {
                // BKPT.
            }
            break;

        case 0x60:
        case 0x61:
        case 0x62:
        case 0x63:
            // STM.
            break;

        case 0x64:
        case 0x65:
        case 0x66:
        case 0x67:
            // LDM/LDMIA/LDMFD.
            break;

        case 0x68:
        case 0x69:
        case 0x6a:
        case 0x6b:
        case 0x6c:
        case 0x6d:
        case 0x6e:
            // B conditional branch.
            break;

        case 0x6f:
            if (instr & 0x0100)
            {
                // SVC.
            }
            else
            {
                // UDF.
            }
            break;

        case 0x70:
        case 0x71:
        case 0x72:
        case 0x73:
            // B unconditional branch.
            break;

        case 0x74:
        case 0x75:
        case 0x76:
        case 0x77:
        case 0x78:
        case 0x79:
        case 0x70:
        case 0x7b:
        case 0x7c:
        case 0x7d:
        case 0x7e:
        case 0x7f:
            // It's a 32-bit ARM instruction.
            execute_instruction32(cpu);
            break;
    }
}
