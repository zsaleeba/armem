#pragma once

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#define REG_SP 13
#define REG_LR 14
#define REG_PC 15
#define NUM_REGISTERS 16

typedef struct 
{
    uint8_t *text;
    size_t   text_size;
    uint8_t *data;
    size_t   data_size;
    uint8_t *rodata;
    size_t   rodata_size;
    uint8_t *bss;
    size_t   bss_size;
    uint8_t *dynamic;
    size_t   dynamic_size;
    uint8_t *hash;
    size_t   hash_size;
    uint8_t *got;
    size_t   got_size;
    uint8_t *plt;
    size_t   plt_size;
} armem_sections_t;


//
// CPU state.
//

typedef struct
{
    void    *ip;                // Instruction pointer.
    uint32_t r[NUM_REGISTERS];  // General purpose registers.
    void    *mem;               // Memory;
} armem_cpu_state_t;


// Prototypes.
bool armem_parse_elf(armem_sections_t *sections, void *buf, size_t len);
void reset_cpu(armem_cpu_state_t *cpu);
void execute_instruction(armem_cpu_state_t *cpu);
uint8_t  mem_read8(armem_cpu_state_t *cpu, uint32_t addr);
uint16_t mem_read16(armem_cpu_state_t *cpu, uint32_t addr);
uint32_t mem_read32(armem_cpu_state_t *cpu, uint32_t addr);
